<?php
session_start();

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>ProjetoWEB</title>

</head>
<body>

<nav class="navbar bg-faded navbar-inverse" style="background-color: #1d1e1f" >

    <a  class="navbar-brand" style="color:white; background-color: #1d1e1f"  >

        CADASTRO DE DEPARTAMENTOS

    </a>
    <ul class="nav navbar-nav">

        <li class="nav-item">
            <a href="index.php" class="nav-link">Home</a>
        </li>

        <li class="nav-item">
            <a href="publico/corpo.php" class="nav-link">Corpo Docente</a>
        </li>
        <li class="nav-item">
            <a href="publico/eventos.php" class="nav-link">Eventos</a>
        </li>
        <li class="nav-item">
            <a href="publico/historia.php" class="nav-link">História do Colégio</a>
        </li>
        <li class="nav-item">
            <a href="http://www.cotil.unicamp.br/" class="nav-link">Site do Cotil</a>
        </li>
        <?php
        if(!isset($_SESSION['usuario'])){
            echo '<li class="nav-item"><a href="login.php" class="nav-link" >Login</a></li>';
            echo '<li class="nav-item"><a href="formulario/registrar.php" class="nav-link" >Registrar</a></li>';
        }else {
            echo '<li class="nav-item"><a href="cadastroDepto.php" class="nav-link" >Cadastro de Departamentos</a></li>';
            echo '<li class="nav-item"><a href="logout.php" class="nav-link" >Logout</a></li>';
        }

        ?>

</nav>
<div class="container">
    <!-- jumbotron -->
    <div class="jumbotron" style="background-color: skyblue;">

<?php
require_once "conexao.php";

$acao = isset($_REQUEST['acao'])?$_REQUEST['acao']:null;
$id = isset($_REQUEST['id'])?$_REQUEST['id']:null;
$sigla = isset($_REQUEST['sigla'])?$_REQUEST['sigla']:null;
$nome = isset($_REQUEST['nome'])?$_REQUEST['nome']:null;
$chefe = isset($_REQUEST['chefe'])?$_REQUEST['chefe']:null;

$erro = null;

if($acao=="editar"){
    $sql = "SELECT * FROM departamentos WHERE id=?";
    $rs = $cn->prepare($sql);
    $rs->bindParam(1, $id);
    if($rs->execute()){
        $depto = $rs->fetch(PDO::FETCH_OBJ);
        $sigla = $depto->sigla;
        $nome = $depto->nome;
        $chefe = $depto->chefe;
    }else{
        $erro = "Falha ao acessa Banco de Dados!";
        echo $erro;
        print "<meta http-equiv='refresh' content='1;url=cadDepto.php'>";
        exit(0);
    }
}elseif($acao=="atualizar"){
    $sql = "UPDATE departamentos ";
    $sql .= "SET sigla=:sigla, nome=:nome, chefe=:chefe ";
    $sql .= "WHERE id=:id";
    $stmt = $cn->prepare($sql);
    $stmt->bindParam("nome",$nome);
    $stmt->bindParam("sigla",$sigla);
    $stmt->bindParam("chefe",$chefe);
    $stmt->bindParam("id",$id);
    if ($stmt->execute()) {
        $erro = "Departamento atualizado com sucesso!";
        echo $erro;
        print "<meta http-equiv='refresh' content='1;url=cadastroDepto.php'>";
        exit(0);
    } else {
        $erro = implode(",", $stmt->errorInfo());
    }
} else {
    $erro = "Dados inválidos!";
}
?>

<div class="container">
    <h1>Edição de Departamentos</h1>
    <hr>
    <form action="?acao=atualizar" method="post">
        <input type="hidden" name="id" value="<?php echo $id;?>">
        Sigla:<br>
        <input type="text"
               name="sigla"
               size="10"
               value="<?php echo $sigla;?>"
               maxlength="50">
        <br>
        <br>
        Nome:<br>
        <input type="text"
               name="nome"
               size="50"
               value="<?php echo $nome;?>"
               maxlength="50">
        <br>
        <br>
        Chefe:<br>
        <input type="text"
               name="chefe"
               size="50"
               value="<?php echo $chefe;?>"
               maxlength="50"> <br>
        <br>
        <br>
        <input type="submit" name="btnAtualiza" value="Atualizar" class="btn btn-info btn-lg">
        <input type="button" name="btnCancelar" value="Cancelar" class="btn btn-info btn-lg" onclick="history.back()">
    </form>
    <?php
    if($erro!=null){
        echo $erro;
    }
    ?>
</div>


    <br>
    <br>
</div>
    <hr>

    <br>
</div>

<br><br>
<footer class="navbar" style="padding-top: 0px; margin-top: 0px; background-color: skyblue">
    <br>
    <p style="font-size: medium; color: black" align="center">
        Projeto DinfoApp<br>
        Disciplina: Desenvolvimento de Aplicações WEB<br>
        Professor: José Alberto Matioli<br>
        Equipe de desenvolvimento:<br>
        Aluna: Julia De Nadai<br>
        RA: 16464<br>


    </p>
</footer>

    <script src="assets/js/jquery-3.1.0.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


</body>
</html>