<?php
session_start();

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>ProjetoDinfoApp</title>

</head>
<body>

<nav class="navbar bg-faded navbar-inverse" style="background-color: #1d1e1f" >

    <a  class="navbar-brand" style="color:white; background-color: #1d1e1f"  >

        EDITAR USUÁRIO

    </a>

</nav>
<div class="container">
    <!-- jumbotron -->
    <div class="jumbotron" style="background-color: skyblue;">

        <?php
        require_once "conexao.php";

        $acao = isset($_REQUEST['acao'])?$_REQUEST['acao']:null;
        $id = isset($_REQUEST['id'])?$_REQUEST['id']:null;
        $nome = isset($_REQUEST['nome'])?$_REQUEST['nome']:null;
        $senha = isset($_REQUEST['senha'])?$_REQUEST['senha']:null;

        $erro = null;

        if($acao=="editar"){
            $sql = "SELECT * FROM usuarios WHERE id=?";
            $rs = $cn->prepare($sql);
            $rs->bindParam(1, $id);
            if($rs->execute()){
                $usuario = $rs->fetch(PDO::FETCH_OBJ);
                $nome = $usuario->nome;
                $senha = $usuario->senha;
            }else{
                $erro = "Falha ao acessa Banco de Dados!";
                echo $erro;
                print "<meta http-equiv='refresh' content='1;url=registrar.php'>";
                exit(0);
            }
        }elseif($acao=="atualizar"){
            $sql = "UPDATE usuarios ";
            $sql .= "SET nome=:nome, senha=:senha ";
            $sql .= "WHERE id=:id";
            $stmt = $cn->prepare($sql);
            $stmt->bindParam("nome",$nome);
            $stmt->bindParam("senha",$senha);
            $stmt->bindParam("id",$id);
            if ($stmt->execute()) {
                $erro = "Usuário atualizado com sucesso!";
                echo $erro;
                print "<meta http-equiv='refresh' content='1;url=index.php'>";
                exit(0);
            } else {
                $erro = implode(",", $stmt->errorInfo());
            }
        } else {
            $erro = "Dados inválidos!";
        }
        ?>

        <div class="container">
            <h1>Editar Perfil</h1>
            <hr>
            <form action="?acao=atualizar" method="post">
                <input type="hidden" name="id" value="<?php echo $id;?>">

                <br>
                Nome:<br>
                <input type="text"
                       name="nome"
                       size="100%"
                       value="<?php echo $nome;?>"
                       maxlength="50">
                <br>
                <br>
                Senha:<br>
                <input type="password"
                       name="senha"
                       size="100%"
                       value="<?php echo $senha;?>"
                       maxlength="50"> <br>
                <br>
                <br>
                <input type="submit" name="btnAtualiza" value="Atualizar" class="btn btn-info btn-lg">
                <input type="button" name="btnCancelar" value="Cancelar" class="btn btn-info btn-lg" onclick="history.back()">
            </form>
            <br>
            <?php
            if($erro!=null){
                echo $erro;
            }
            ?>
        </div>


        <br>
    </div>
    <hr>



    <br>
    <br>
</div>
    <br><br>
    <footer class="navbar" style="padding-top: 0px; margin-top: 0px; background-color: skyblue">
        <br>
        <p style="font-size: medium; color: black" align="center">
            Projeto DinfoApp<br>
            Disciplina: Desenvolvimento de Aplicações WEB<br>
            Professor: José Alberto Matioli<br>
            Equipe de desenvolvimento:<br>
            Aluna: Julia De Nadai<br>
            RA: 16464<br>


        </p>
    </footer>

    <script src="assets/js/jquery-3.1.0.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


</body>
</html>