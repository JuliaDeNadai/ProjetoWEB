<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>ProjetoWEB</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
</head>
<body>

<nav class="navbar bg-faded navbar-inverse" style="background-color: #1d1e1f" >

    <a  class="navbar-brand" style="color:white; background-color: #1d1e1f"  >

        REGISTRAR

    </a>
    <ul class="nav navbar-nav">

        <li class="nav-item">
            <a href="../index.php" class="nav-link">Home</a>
        </li>

        <li class="nav-item">
            <a href="../publico/corpo.php" class="nav-link">Corpo Docente</a>
        </li>
        <li class="nav-item">
            <a href="../publico/eventos.php" class="nav-link">Eventos</a>
        </li>
        <li class="nav-item">
            <a href="../publico/historia.php" class="nav-link">História do Colégio</a>
        </li>
        <li class="nav-item">
            <a href="http://www.cotil.unicamp.br/" class="nav-link">Site do Cotil</a>
        </li>
        <?php
        if(!isset($_SESSION['usuario'])){
            echo '<li class="nav-item"><a href="../login.php" class="nav-link" >Login</a></li>';
            echo '<li class="nav-item"><a href="../formulario/registrar.php" class="nav-link" >Registrar</a></li>';
        }else {
            echo '<li class="nav-item"><a href="../cadastroDepto.php" class="nav-link" >Cadastro de Departamentos</a></li>';
            echo '<li class="nav-item"><a href="../logout.php" class="nav-link" >Logout</a></li>';
        }

        ?>
</nav>

<div class="container">

<?php
require_once "../conexao.php";

$acao = isset($_REQUEST['acao'])?$_REQUEST['acao']:null;
$id = isset($_REQUEST['id'])?$_REQUEST['id']:null;
$senha = isset($_REQUEST['senha'])?$_REQUEST['senha']:null;
$senha2 = isset($_REQUEST['senha2'])?$_REQUEST['senha2']:null;
$nome = isset($_REQUEST['nome'])?$_REQUEST['nome']:null;
$email = isset($_REQUEST['email'])?$_REQUEST['email']:null;

$mensagem = array();

?>

<div class="jumbotron" style="background-color: skyblue;">
    <br>
    <h1>Cadastro de Usuários</h1>
    <br>
    <hr>
    <form  action="?acao=incluir" method="post">

        <p>Digite seu nome:</p>
        <input  type="text" name="nome" size="50" maxlength="50" > <br><br>

        <p>Digite seu email:</p>
        <input  type="email" name="email" size="50" maxlength="100"> <br><br>

        <p>Digite sua senha:</p>
        <input type="password" name="senha" size="50" maxlength="50" > <br><br>

        <p>Digite sua senha novamente:</p>
        <input type="password" name="senha2" size="50"  > <br><br>
        <button type="submit" class="btn btn-info btn-lg" >ENVIAR</button>

    </form>
    <br>
    <?php


    if($acao=="incluir"){

        $erros=false;
        if(isset($_REQUEST['nome']) && ($_REQUEST['nome']!='') ){
            $nome = $_REQUEST['nome'];
        }else{
            echo "Campo NOME não pode ser vazio!<br>";
            $erros=true;
            exit();
        }


        if(isset($_REQUEST['email'])  && ($_REQUEST['email']!='') ){
            $email = $_REQUEST['email'];
        }else{
            echo "Campo EMAIL não pode ser vazio!<br>";
            $erros=true;
            exit();
        }

        if(isset($_REQUEST['senha'])  && ($_REQUEST['senha']!='') ){
            $senha = $_REQUEST['senha'];
        }else{
            echo "Campo SENHA não pode ser vazio!<br>";
            $erros=true;
            exit();
        }

        if(isset($_REQUEST['senha2'])  && ($_REQUEST['senha2']!='') ){
            $senha2 = $_REQUEST['senha2'];
            if($senha!=$senha2){
                echo "Senhas não conferem!<br>";
                $erros=true;
            }
        }else{
            $erro = $stmt->errorCode();
            $mensagem[$erro] = implode(",", $stmt->errorInfo());
            $erros=true;
        }


        if(!$erros) {
            $sql = "INSERT INTO usuarios ";
            $sql .= "(id,nome, email, senha)";
            $sql .= " VALUES (?,?,?,?) ";
            $stmt = $cn->prepare($sql);
            $stmt->bindParam(1, $id);
            $stmt->bindParam(2, $nome);
            $stmt->bindParam(3, $email);
            $stmt->bindParam(4, $senha);
            if ($stmt->execute()) {
                $mensagemEmail = 'Olá, este é um email enviado pelo ProjetoDinfoApp.';
                mail($email,'Email de verificação',$mensagemEmail);

                $erro = "Usuário cadastrado com sucesso!";
                echo $erro;
                exit(0);
            } else {
                $erro = $stmt->errorCode();
                $mensagem[$erro] = implode(",", $stmt->errorInfo());
            }

        }
        ?>

<?php
    }elseif($acao=="excluir"){
        $sql = "DELETE FROM usuarios WHERE id=?";
        $stmt = $cn->prepare($sql);
        $stmt->bindParam(1,$id);
        if ($stmt->execute()) {
            $erro = $stmt->errorCode();
            $mensagem[$erro] = "Usuário excluido com sucesso!";
        } else {
            $erro = $stmt->errorCode();
            $mensagem[$erro] = implode(",", $stmt->errorInfo());
        }
    }

    $usuarios =($cn->query("SELECT * FROM usuarios"))->fetchAll(PDO::FETCH_OBJ);

    ?>
<br>


<br>


    <?php
    if(count($mensagem)>0){
        foreach ($mensagem as $msg){
            echo $msg;
        }
    }
    ?>

</div>
    <br>
    <hr>
    <table class="table table-striped table-bordered table">
        <tr>
            <th>Nome</th>
            <th>Email</th>
            <th>ID</th>
            <th>Ações</th>
        </tr>
        <?php
        if($usuarios){
            foreach ($usuarios as $usuario){
                ?>
                <tr>
                    <td><?php echo $usuario->nome;?></td>
                    <td><?php echo $usuario->email;?></td>
                    <td><?php echo $usuario->id;?></td>
                    <td>
                        <a href="../editaUsuario.php?acao=editar&id=<?php echo $usuario->id;?>">Editar</a>
                        <a href="registrar.php?acao=excluir&id=<?php echo $usuario->id;?>">Excluir</a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>

    </table>

    <hr>

    <br>

</div>

<br><br>
<footer class="navbar" style="padding-top: 0px; margin-top: 0px; background-color: skyblue">
    <br>
    <p style="font-size: medium; color: black" align="center">
        Projeto DinfoApp<br>
        Disciplina: Desenvolvimento de Aplicações WEB<br>
        Professor: José Alberto Matioli<br>
        Equipe de desenvolvimento:<br>
        Aluna: Julia De Nadai<br>
        RA: 16464<br>


    </p>
</footer>

<script src="../assets/js/jquery-3.1.0.js"></script>
<script src="../assets/js/bootstrap.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>

