<?php
session_start();
?>
<!doctype html>
<html lang="eng">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ProjetoDinfoApp</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>

<nav class="navbar bg-faded navbar-inverse"  >

    <a  class="navbar-brand" style="color:white; background-color: #1d1e1f"  >

        LOGIN

    </a>
    <ul class="nav navbar-nav">

        <li class="nav-item">
            <a href="index.php" class="nav-link">Home</a>
        </li>

        <li class="nav-item">
            <a href="publico/corpo.php" class="nav-link">Corpo Docente</a>
        </li>
        <li class="nav-item">
            <a href="publico/eventos.php" class="nav-link">Eventos</a>
        </li>
        <li class="nav-item">
            <a href="publico/historia.php" class="nav-link">História do Colégio</a>
        </li>
        <li class="nav-item">
            <a href="http://www.cotil.unicamp.br/" class="nav-link">Site do Cotil</a>
        </li>
        <?php
        if(!isset($_SESSION['usuario'])){
            echo '<li class="nav-item"><a href="formulario/registrar.php">Registrar</a></li>';
        }else {
            echo '<li class="nav-item"><a href="cadastroDepto.php" class="btn btn-info btn-lg">Cadastro de Departamentos</a></li>';
            echo '<li class="nav-item"><a href="logout.php" class="btn btn-info btn-lg" >Logout</a></li>';
        }

        ?>

</nav>

<?php
require_once  "conexao.php";

$acao = isset($_REQUEST['acao'])?$_REQUEST['acao']:null;
$id = isset($_REQUEST['id'])?$_REQUEST['id']:null;
$senha = isset($_POST['senha']);
$email = isset($_POST['email']);


?>



<div class="container">
    <div class="jumbotron" style="background-color: skyblue;">
    <h1>Login </h1>
        <br>
        <hr>
        <?php

        if($acao=="autenticar"){
            $sql =("SELECT * FROM usuarios WHERE email= $email AND senha = $senha");
            $stmt = $cn->prepare($sql);

            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':senha', $senha);

            $stmt->execute();

            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($users > 0){

                $_SESSION['usuario']= 'Usuário';
                $_SESSION['email']=$_POST['email'];
                $_SESSION['senha']=$_POST['senha'];
                $erro = "Usuário logado com sucesso!";
                echo $erro;
                print "<meta http-equiv='refresh' content='1;url=index.php'>";
                exit();
            }else{
                echo "Usuário ou senha inválidos";
                die();
            }
        }

        ?>

        <form action="?acao=autenticar" method="post" >

        <p>Digite seu email:</p>
            <input type="email" size="100%" name="email" id="email" >
            <br>
            <br>
            <p>Digite sua senha:</p>
            <input type="password" size="100%" name="senha" id="senha" >
            <br>
            <br>
            <input type="submit" name="btnEnviar" class="btn btn-info btn-lg">
            <br>
            <br>
            <?php
            if(!isset($_SESSION['usuario'])){
                echo '<a href="formulario/registrar.php" class="btn btn-info btn-lg">Ainda não sou cadastrado</a>';
            }
            ?>

        </form>
        <br>


    </div>
    <hr>


</div>
<br><br>
<footer class="navbar" style="padding-top: 0px; margin-top: 0px; background-color: skyblue">
    <br>
    <p style="font-size: medium; color: black" align="center">
        Projeto DinfoApp<br>
        Disciplina: Desenvolvimento de Aplicações WEB<br>
        Professor: José Alberto Matioli<br>
        Equipe de desenvolvimento:<br>
        Aluna: Julia De Nadai<br>
        RA: 16464<br>


    </p>
</footer>

<script src="assets/js/jquery-3.1.0.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>