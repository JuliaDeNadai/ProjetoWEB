<?php
session_start();

?>
<!doctype html>
<html lang="eng">
<head>
    <meta charset="utf-8">
    <title>ProjetoWEB</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
</head>
<body>


<nav class="navbar bg-faded navbar-inverse" style="background-color: #1d1e1f" >

    <a  class="navbar-brand" style="color:white; background-color: #1d1e1f"  >

        CADASTRO DE DEPARTAMENTOS

    </a>
    <ul class="nav navbar-nav">

        <li class="nav-item">
            <a href="index.php" class="nav-link">Home</a>
        </li>

        <li class="nav-item">
            <a href="publico/corpo.php" class="nav-link">Corpo Docente</a>
        </li>
        <li class="nav-item">
            <a href="publico/eventos.php" class="nav-link">Eventos</a>
        </li>
        <li class="nav-item">
            <a href="publico/historia.php" class="nav-link">História do Colégio</a>
        </li>
        <li class="nav-item">
            <a href="http://www.cotil.unicamp.br/" class="nav-link">Site do Cotil</a>
        </li>
        <?php
        if(!isset($_SESSION['usuario'])){
            echo '<li class="nav-item"><a href="login.php" class="nav-link" >Login</a></li>';
            echo '<li class="nav-item"><a href="formulario/registrar.php" class="nav-link" >Registrar</a></li>';
        }else {
            echo '<li class="nav-item"><a href="cadastroDepto.php" class="nav-link" >Cadastro de Departamentos</a></li>';
            echo '<li class="nav-item"><a href="logout.php" class="nav-link" >Logout</a></li>';
        }

        ?>

</nav>
<?php
require_once "conexao.php";

$acao = isset($_REQUEST['acao'])?$_REQUEST['acao']:null;
$id = isset($_REQUEST['id'])?$_REQUEST['id']:null;
$sigla = isset($_REQUEST['sigla'])?$_REQUEST['sigla']:null;
$nome = isset($_REQUEST['nome'])?$_REQUEST['nome']:null;
$chefe = isset($_REQUEST['chefe'])?$_REQUEST['chefe']:null;

$mensagem = array();

if($acao=="incluir"){
    $sql = "INSERT INTO departamentos ";
    $sql .= "(sigla, nome, chefe) ";
    $sql .= " VALUES (?,?,?) ";
    $stmt = $cn->prepare($sql);
    $stmt->bindParam(1, $sigla);
    $stmt->bindParam(2, $nome);
    $stmt->bindParam(3, $chefe);
    if ($stmt->execute()) {
        $erro = $stmt->errorCode();
        $mensagem[$erro] = "Departamento criado com sucesso!";
    } else {
        $erro = $stmt->errorCode();
        $mensagem[$erro] = implode(",", $stmt->errorInfo());
    }
}elseif($acao=="excluir"){
    $sql = "DELETE FROM departamentos WHERE id=?";
    $stmt = $cn->prepare($sql);
    $stmt->bindParam(1,$id);
    if ($stmt->execute()) {
        $erro = $stmt->errorCode();
        $mensagem[$erro] = "Departamento excluido com sucesso!";
    } else {
        $erro = $stmt->errorCode();
        $mensagem[$erro] = implode(",", $stmt->errorInfo());
    }
}

//$stmt = $cn->query("SELECT * FROM departamentos");
$deptos =($cn->query("SELECT * FROM departamentos"))->fetchAll(PDO::FETCH_OBJ);
?>

<br>

<div class="container">

    <div class="jumbotron" style="background-color: skyblue;">
        <h1>Cadastro de Departamentos</h1>
        <hr>
        <form action="?acao=incluir" method="post">
            Sigla:<br>
            <input type="text"
                   name="sigla"
                   size="10%"
                   maxlength="50">
            <BR>
            Nome:<br>
            <input type="text"
                   name="nome"
                   size="27%"
                   maxlength="50">
            <br>

            Chefe:<br>
            <input type="text"
                   name="chefe"
                   size="27%"
                   maxlength="50"> <br>
            <br>
            <input type="submit" name="btnEnviar" value="Enviar" class="btn btn-info btn-lg">
        </form>
    </div>
    <?php
    if(count($mensagem)>0){
        foreach ($mensagem as $msg){
            echo $msg;
        }
    }
    ?>

<hr>
    <table class="table table-striped table-bordered table">
        <tr>
            <th>Sigla</th>
            <th>Nome</th>
            <th>Chefe</th>
            <th>Ações</th>
        </tr>
        <?php
        if($deptos){
            foreach ($deptos as $depto){
                ?>
                <tr>
                    <td><?php echo $depto->sigla;?></td>
                    <td><?php echo $depto->nome;?></td>
                    <td><?php echo $depto->chefe;?></td>
                    <td>
                        <a href="editaDepto.php?acao=editar&id=<?php echo $depto->id;?>">Editar</a>
                        <a href="cadastroDepto.php?acao=excluir&id=<?php echo $depto->id;?>">Excluir</a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>

    </table>

<br>
</div>
<br><br>
<footer class="navbar" style="padding-top: 0px; margin-top: 0px; background-color: skyblue">
    <br>
    <p style="font-size: medium; color: black" align="center">
        Projeto DinfoApp<br>
        Disciplina: Desenvolvimento de Aplicações WEB<br>
        Professor: José Alberto Matioli<br>
        Equipe de desenvolvimento:<br>
        Aluna: Julia De Nadai<br>
        RA: 16464<br>


    </p>
</footer>

    <script src="assets/js/jquery-3.1.0.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>