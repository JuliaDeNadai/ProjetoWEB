<?php
//inicializando o serviço de sessao
session_start();
require_once  "conexao.php";

$id = isset($_REQUEST['id'])?$_REQUEST['id']:null;
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">

    <title>ProjetoDinfoApp</title>
</head>

<body>

<nav class="navbar bg-faded navbar-inverse" style="background-color: #1d1e1f" >

    <a  class="navbar-brand" style="color:white; background-color: #1d1e1f"  >

        BEM - VINDO

    </a>
    <ul class="nav navbar-nav">

        <li class="nav-item">
            <a href="index.php" class="nav-link">Home</a>
        </li>

        <li class="nav-item">
            <a href="publico/corpo.php" class="nav-link">Corpo Docente</a>
        </li>
        <li class="nav-item">
            <a href="publico/eventos.php" class="nav-link">Eventos</a>
        </li>
        <li class="nav-item">
            <a href="publico/historia.php" class="nav-link">História do Colégio</a>
        </li>
        <li class="nav-item">
            <a href="http://www.cotil.unicamp.br/" class="nav-link">Site do Cotil</a>
        </li>
        <?php
        if(!isset($_SESSION['usuario'])){
            echo '<li class="nav-item"><a href="login.php" class="nav-link" >Login</a></li>';
            echo '<li class="nav-item"><a href="formulario/registrar.php" class="nav-link" >Registrar</a></li>';
        }else {
            echo '<li class="nav-item"><a href="cadastroDepto.php" class="nav-link" >Cadastro de Departamentos</a></li>';
            echo '<li class="nav-item"><a href="logout.php" class="nav-link" >Logout</a></li>';
        }

        ?>

</nav>

<div class="container">
<!-- jumbotron -->
<div class="jumbotron" style="background-color: skyblue;">

    <h1 class="display-3">Departamentos</h1>

    <p class="lead">várias coisas aleatórias pro usuário ler</p>

    <hr class="my-5">

    <p>Esse é um pequeno parágrafo.</p>

</div>
    <hr>
<!-- fim jumbotron -->



</div>
<br>
<br>
<br>
<footer class="navbar" style="padding-top: 0px; margin-top: 0px; background-color: skyblue">
    <p style="font-size: medium; color: black" align="center">
        Projeto DinfoApp<br>
        Disciplina: Desenvolvimento de Aplicações WEB<br>
        Professor: José Alberto Matioli<br>
        Equipe de desenvolvimento:<br>
        Aluna: Julia De Nadai<br>
        RA: 16464<br>


    </p>
</footer>

<script src="assets/js/jquery-3.1.0.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>

</html>